{
  "version": "1.2",
  "package": {
    "name": "",
    "version": "",
    "description": "",
    "author": "",
    "image": ""
  },
  "design": {
    "board": "TinyFPGA-BX",
    "graph": {
      "blocks": [
        {
          "id": "bd769860-0ed7-4e7d-bd1e-f922cc39ee22",
          "type": "basic.input",
          "data": {
            "name": "DIN",
            "virtual": true,
            "range": "[31:0]",
            "pins": [
              {
                "index": "31",
                "name": "NULL",
                "value": "NULL"
              },
              {
                "index": "30",
                "name": "NULL",
                "value": "NULL"
              },
              {
                "index": "29",
                "name": "NULL",
                "value": "NULL"
              },
              {
                "index": "28",
                "name": "NULL",
                "value": "NULL"
              },
              {
                "index": "27",
                "name": "NULL",
                "value": "NULL"
              },
              {
                "index": "26",
                "name": "NULL",
                "value": "NULL"
              },
              {
                "index": "25",
                "name": "NULL",
                "value": "NULL"
              },
              {
                "index": "24",
                "name": "NULL",
                "value": "NULL"
              },
              {
                "index": "23",
                "name": "NULL",
                "value": "NULL"
              },
              {
                "index": "22",
                "name": "NULL",
                "value": "NULL"
              },
              {
                "index": "21",
                "name": "NULL",
                "value": "NULL"
              },
              {
                "index": "20",
                "name": "NULL",
                "value": "NULL"
              },
              {
                "index": "19",
                "name": "NULL",
                "value": "NULL"
              },
              {
                "index": "18",
                "name": "NULL",
                "value": "NULL"
              },
              {
                "index": "17",
                "name": "NULL",
                "value": "NULL"
              },
              {
                "index": "16",
                "name": "NULL",
                "value": "NULL"
              },
              {
                "index": "15",
                "name": "NULL",
                "value": "NULL"
              },
              {
                "index": "14",
                "name": "NULL",
                "value": "NULL"
              },
              {
                "index": "13",
                "name": "NULL",
                "value": "NULL"
              },
              {
                "index": "12",
                "name": "NULL",
                "value": "NULL"
              },
              {
                "index": "11",
                "name": "NULL",
                "value": "NULL"
              },
              {
                "index": "10",
                "name": "NULL",
                "value": "NULL"
              },
              {
                "index": "9",
                "name": "NULL",
                "value": "NULL"
              },
              {
                "index": "8",
                "name": "NULL",
                "value": "NULL"
              },
              {
                "index": "7",
                "name": "NULL",
                "value": "NULL"
              },
              {
                "index": "6",
                "name": "NULL",
                "value": "NULL"
              },
              {
                "index": "5",
                "name": "NULL",
                "value": "NULL"
              },
              {
                "index": "4",
                "name": "NULL",
                "value": "NULL"
              },
              {
                "index": "3",
                "name": "NULL",
                "value": "NULL"
              },
              {
                "index": "2",
                "name": "NULL",
                "value": "NULL"
              },
              {
                "index": "1",
                "name": "NULL",
                "value": "NULL"
              },
              {
                "index": "0",
                "name": "NULL",
                "value": "NULL"
              }
            ],
            "clock": false
          },
          "position": {
            "x": 336,
            "y": 296
          }
        },
        {
          "id": "ce3ba22e-b85f-4c86-a6b5-5bc964bf3293",
          "type": "basic.output",
          "data": {
            "name": "SDOP",
            "virtual": true,
            "pins": [
              {
                "index": "0",
                "name": "NULL",
                "value": "NULL"
              }
            ]
          },
          "position": {
            "x": 1160,
            "y": 336
          }
        },
        {
          "id": "031afb48-d6ec-4994-b07d-961f865cc3b8",
          "type": "basic.input",
          "data": {
            "name": "CLK",
            "virtual": true,
            "pins": [
              {
                "index": "0",
                "name": "NULL",
                "value": "NULL"
              }
            ],
            "clock": true
          },
          "position": {
            "x": 344,
            "y": 448
          }
        },
        {
          "id": "e5a73377-e314-4ff8-848f-33ba6ecb09de",
          "type": "basic.output",
          "data": {
            "name": "SDON",
            "virtual": true,
            "pins": [
              {
                "index": "0",
                "name": "NULL",
                "value": "NULL"
              }
            ]
          },
          "position": {
            "x": 1160,
            "y": 568
          }
        },
        {
          "id": "b546f314-d238-4bc4-954e-014fc09f612b",
          "type": "basic.input",
          "data": {
            "name": "LATCH",
            "virtual": true,
            "pins": [
              {
                "index": "0",
                "name": "NULL",
                "value": "NULL"
              }
            ],
            "clock": false
          },
          "position": {
            "x": 352,
            "y": 600
          }
        },
        {
          "id": "5d05b506-fb60-46f1-bcde-004c732834cc",
          "type": "basic.code",
          "data": {
            "ports": {
              "in": [
                {
                  "name": "DIN",
                  "range": "[31:0]",
                  "size": 32
                },
                {
                  "name": "CLK"
                },
                {
                  "name": "LATCH"
                }
              ],
              "out": [
                {
                  "name": "SDOP"
                },
                {
                  "name": "SDON"
                }
              ]
            },
            "params": [],
            "code": "reg [31:0] shreg = 0;\ninteger idx = 31;\nbool zerostuff = 0;\nreg sdopos, sdoneg;\n\nalways @(posedge LATCH)\n    shreg = DIN;\n\nalways @(posedge CLK)\nbegin\n    if (!zerostuff) begin\n        sdopos = shreg[idx];\n        sdoneg = ~sdopos;\n        if (idx == 0) begin\n            idx = 31;\n        end else begin\n            idx = idx - 1;\n        end\n    end else begin\n        sdopos = 0;\n        sdoneg = 0;\n    end\n    zerostuff = ~zerostuff;\nend\n\nassign SDOP = sdopos;\nassign SDON = sdoneg;\n"
          },
          "position": {
            "x": 536,
            "y": 248
          },
          "size": {
            "width": 560,
            "height": 464
          }
        }
      ],
      "wires": [
        {
          "source": {
            "block": "b546f314-d238-4bc4-954e-014fc09f612b",
            "port": "out"
          },
          "target": {
            "block": "5d05b506-fb60-46f1-bcde-004c732834cc",
            "port": "LATCH"
          }
        },
        {
          "source": {
            "block": "031afb48-d6ec-4994-b07d-961f865cc3b8",
            "port": "out"
          },
          "target": {
            "block": "5d05b506-fb60-46f1-bcde-004c732834cc",
            "port": "CLK"
          }
        },
        {
          "source": {
            "block": "bd769860-0ed7-4e7d-bd1e-f922cc39ee22",
            "port": "out"
          },
          "target": {
            "block": "5d05b506-fb60-46f1-bcde-004c732834cc",
            "port": "DIN"
          },
          "size": 32
        },
        {
          "source": {
            "block": "5d05b506-fb60-46f1-bcde-004c732834cc",
            "port": "SDON"
          },
          "target": {
            "block": "e5a73377-e314-4ff8-848f-33ba6ecb09de",
            "port": "in"
          }
        },
        {
          "source": {
            "block": "5d05b506-fb60-46f1-bcde-004c732834cc",
            "port": "SDOP"
          },
          "target": {
            "block": "ce3ba22e-b85f-4c86-a6b5-5bc964bf3293",
            "port": "in"
          }
        }
      ]
    }
  },
  "dependencies": {}
}