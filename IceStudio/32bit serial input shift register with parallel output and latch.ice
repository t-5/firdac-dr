{
  "version": "1.2",
  "package": {
    "name": "",
    "version": "",
    "description": "",
    "author": "",
    "image": ""
  },
  "design": {
    "board": "TinyFPGA-BX",
    "graph": {
      "blocks": [
        {
          "id": "525c1541-365e-485b-82d4-1946acae1069",
          "type": "basic.input",
          "data": {
            "name": "CLK",
            "virtual": true,
            "pins": [
              {
                "index": "0",
                "name": "NULL",
                "value": "NULL"
              }
            ],
            "clock": true
          },
          "position": {
            "x": 296,
            "y": -112
          }
        },
        {
          "id": "7a423093-735f-42db-920f-4a6543a4c475",
          "type": "basic.output",
          "data": {
            "name": "DOUT",
            "virtual": true,
            "range": "[31:0]",
            "pins": [
              {
                "index": "31",
                "name": "NULL",
                "value": "NULL"
              },
              {
                "index": "30",
                "name": "NULL",
                "value": "NULL"
              },
              {
                "index": "29",
                "name": "NULL",
                "value": "NULL"
              },
              {
                "index": "28",
                "name": "NULL",
                "value": "NULL"
              },
              {
                "index": "27",
                "name": "NULL",
                "value": "NULL"
              },
              {
                "index": "26",
                "name": "NULL",
                "value": "NULL"
              },
              {
                "index": "25",
                "name": "NULL",
                "value": "NULL"
              },
              {
                "index": "24",
                "name": "NULL",
                "value": "NULL"
              },
              {
                "index": "23",
                "name": "NULL",
                "value": "NULL"
              },
              {
                "index": "22",
                "name": "NULL",
                "value": "NULL"
              },
              {
                "index": "21",
                "name": "NULL",
                "value": "NULL"
              },
              {
                "index": "20",
                "name": "NULL",
                "value": "NULL"
              },
              {
                "index": "19",
                "name": "NULL",
                "value": "NULL"
              },
              {
                "index": "18",
                "name": "NULL",
                "value": "NULL"
              },
              {
                "index": "17",
                "name": "NULL",
                "value": "NULL"
              },
              {
                "index": "16",
                "name": "NULL",
                "value": "NULL"
              },
              {
                "index": "15",
                "name": "NULL",
                "value": "NULL"
              },
              {
                "index": "14",
                "name": "NULL",
                "value": "NULL"
              },
              {
                "index": "13",
                "name": "NULL",
                "value": "NULL"
              },
              {
                "index": "12",
                "name": "NULL",
                "value": "NULL"
              },
              {
                "index": "11",
                "name": "NULL",
                "value": "NULL"
              },
              {
                "index": "10",
                "name": "NULL",
                "value": "NULL"
              },
              {
                "index": "9",
                "name": "NULL",
                "value": "NULL"
              },
              {
                "index": "8",
                "name": "NULL",
                "value": "NULL"
              },
              {
                "index": "7",
                "name": "NULL",
                "value": "NULL"
              },
              {
                "index": "6",
                "name": "NULL",
                "value": "NULL"
              },
              {
                "index": "5",
                "name": "NULL",
                "value": "NULL"
              },
              {
                "index": "4",
                "name": "NULL",
                "value": "NULL"
              },
              {
                "index": "3",
                "name": "NULL",
                "value": "NULL"
              },
              {
                "index": "2",
                "name": "NULL",
                "value": "NULL"
              },
              {
                "index": "1",
                "name": "NULL",
                "value": "NULL"
              },
              {
                "index": "0",
                "name": "NULL",
                "value": "NULL"
              }
            ]
          },
          "position": {
            "x": 1120,
            "y": -80
          }
        },
        {
          "id": "4f33eacd-d0a7-4574-8800-d3b32b422580",
          "type": "basic.input",
          "data": {
            "name": "SDI",
            "virtual": true,
            "pins": [
              {
                "index": "0",
                "name": "NULL",
                "value": "NULL"
              }
            ],
            "clock": false
          },
          "position": {
            "x": 296,
            "y": 24
          }
        },
        {
          "id": "8a012405-7f39-478d-9f01-9f7be3cabae6",
          "type": "basic.output",
          "data": {
            "name": "SDO",
            "virtual": true,
            "pins": [
              {
                "index": "0",
                "name": "NULL",
                "value": "NULL"
              }
            ]
          },
          "position": {
            "x": 1120,
            "y": 128
          }
        },
        {
          "id": "4ffbf419-dc95-47cd-8731-3b15dee38989",
          "type": "basic.input",
          "data": {
            "name": "LATCH",
            "virtual": true,
            "pins": [
              {
                "index": "0",
                "name": "NULL",
                "value": "NULL"
              }
            ],
            "clock": false
          },
          "position": {
            "x": 296,
            "y": 160
          }
        },
        {
          "id": "46c50af6-adb1-405b-9a6a-526b274a0d6a",
          "type": "basic.code",
          "data": {
            "ports": {
              "in": [
                {
                  "name": "CLK"
                },
                {
                  "name": "SDI"
                },
                {
                  "name": "LATCH"
                }
              ],
              "out": [
                {
                  "name": "DOUT",
                  "range": "[31:0]",
                  "size": 32
                },
                {
                  "name": "SDO"
                }
              ]
            },
            "params": [],
            "code": "reg [31:0] shreg = 0;\nreg [31:0] outreg = 0;\n\nalways @(posedge CLK)\nbegin\n    shreg = {shreg[30:0], SDI};\nend\n\nalways @(posedge LATCH)\nbegin\n    outreg = shreg;\nend\n\nassign SDO = shreg[31];\nassign DOUT = outreg;\n"
          },
          "position": {
            "x": 480,
            "y": -152
          },
          "size": {
            "width": 528,
            "height": 416
          }
        }
      ],
      "wires": [
        {
          "source": {
            "block": "4ffbf419-dc95-47cd-8731-3b15dee38989",
            "port": "out"
          },
          "target": {
            "block": "46c50af6-adb1-405b-9a6a-526b274a0d6a",
            "port": "LATCH"
          }
        },
        {
          "source": {
            "block": "4f33eacd-d0a7-4574-8800-d3b32b422580",
            "port": "out"
          },
          "target": {
            "block": "46c50af6-adb1-405b-9a6a-526b274a0d6a",
            "port": "SDI"
          }
        },
        {
          "source": {
            "block": "525c1541-365e-485b-82d4-1946acae1069",
            "port": "out"
          },
          "target": {
            "block": "46c50af6-adb1-405b-9a6a-526b274a0d6a",
            "port": "CLK"
          }
        },
        {
          "source": {
            "block": "46c50af6-adb1-405b-9a6a-526b274a0d6a",
            "port": "DOUT"
          },
          "target": {
            "block": "7a423093-735f-42db-920f-4a6543a4c475",
            "port": "in"
          },
          "size": 32
        },
        {
          "source": {
            "block": "46c50af6-adb1-405b-9a6a-526b274a0d6a",
            "port": "SDO"
          },
          "target": {
            "block": "8a012405-7f39-478d-9f01-9f7be3cabae6",
            "port": "in"
          }
        }
      ]
    }
  },
  "dependencies": {}
}