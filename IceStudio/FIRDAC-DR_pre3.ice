{
  "version": "1.2",
  "package": {
    "name": "Led on",
    "version": "1.0.0",
    "description": "",
    "author": "",
    "image": ""
  },
  "design": {
    "board": "TinyFPGA-BX",
    "graph": {
      "blocks": [
        {
          "id": "12a66937-3452-4cb7-8712-72dd77287bce",
          "type": "basic.input",
          "data": {
            "name": "MCLK",
            "virtual": false,
            "pins": [
              {
                "index": "0",
                "name": "PIN_1",
                "value": "A2"
              }
            ],
            "clock": true
          },
          "position": {
            "x": -760,
            "y": -208
          }
        },
        {
          "id": "5b5f2c70-89c8-4f1c-bb2e-99891dbac1ef",
          "type": "basic.input",
          "data": {
            "name": "LRCK",
            "virtual": false,
            "pins": [
              {
                "index": "0",
                "name": "PIN_2",
                "value": "A1"
              }
            ],
            "clock": true
          },
          "position": {
            "x": -760,
            "y": -88
          }
        },
        {
          "id": "9c905044-cfbc-415d-8ced-081c1c4a06a3",
          "type": "basic.input",
          "data": {
            "name": "BCK",
            "virtual": false,
            "pins": [
              {
                "index": "0",
                "name": "PIN_3",
                "value": "B1"
              }
            ],
            "clock": true
          },
          "position": {
            "x": -760,
            "y": 152
          }
        },
        {
          "id": "8ef3e574-76c4-458d-8856-9083a04d7f8c",
          "type": "basic.output",
          "data": {
            "name": "LSDOP",
            "virtual": false,
            "pins": [
              {
                "index": "0",
                "name": "PIN_6",
                "value": "D2"
              }
            ]
          },
          "position": {
            "x": 64,
            "y": 208
          }
        },
        {
          "id": "53cf65c7-3c3d-4f50-99d3-c42fb4dfef36",
          "type": "basic.input",
          "data": {
            "name": "DATA",
            "virtual": false,
            "pins": [
              {
                "index": "0",
                "name": "PIN_4",
                "value": "C2"
              }
            ],
            "clock": false
          },
          "position": {
            "x": -760,
            "y": 240
          }
        },
        {
          "id": "340fe403-9ce9-4d94-97db-e1cafbe5ee46",
          "type": "basic.output",
          "data": {
            "name": "LSDON",
            "virtual": false,
            "pins": [
              {
                "index": "0",
                "name": "PIN_7",
                "value": "D1"
              }
            ]
          },
          "position": {
            "x": 64,
            "y": 288
          }
        },
        {
          "id": "eaa8f799-4f24-453b-8d0d-bcccb74d566a",
          "type": "basic.output",
          "data": {
            "name": "RSDOP",
            "virtual": false,
            "pins": [
              {
                "index": "0",
                "name": "PIN_8",
                "value": "E2"
              }
            ]
          },
          "position": {
            "x": 64,
            "y": 456
          }
        },
        {
          "id": "7e1734b3-7da2-4749-89b0-e3ae3f38cc61",
          "type": "basic.output",
          "data": {
            "name": "RSDON",
            "virtual": false,
            "pins": [
              {
                "index": "0",
                "name": "PIN_9",
                "value": "E1"
              }
            ]
          },
          "position": {
            "x": 64,
            "y": 536
          }
        },
        {
          "id": "2f8b4fc7-efe6-4b68-ad9b-191b1125f3c5",
          "type": "basic.info",
          "data": {
            "info": "MCLK\n= 24.576 MHz\n= 128 FS\n\nFS = 192kHz",
            "readonly": true
          },
          "position": {
            "x": -760,
            "y": -288
          },
          "size": {
            "width": 136,
            "height": 88
          }
        },
        {
          "id": "29197b10-b895-4e81-9167-3dbe1bf68323",
          "type": "basic.info",
          "data": {
            "info": "LRCK = 192kHz",
            "readonly": true
          },
          "position": {
            "x": -760,
            "y": -112
          },
          "size": {
            "width": 128,
            "height": 40
          }
        },
        {
          "id": "d5edc2bf-21fc-42c5-a2b1-9cfa9bd4cf6c",
          "type": "basic.info",
          "data": {
            "info": "BCK\n= 12.288 MHz",
            "readonly": true
          },
          "position": {
            "x": -760,
            "y": 128
          },
          "size": {
            "width": 136,
            "height": 48
          }
        },
        {
          "id": "671b4818-4d67-4d71-b056-e32771954a6d",
          "type": "3676a00f3a70e406487ed14b901daf3e4984e63d",
          "position": {
            "x": -480,
            "y": -88
          },
          "size": {
            "width": 96,
            "height": 64
          }
        },
        {
          "id": "52d3b954-f80b-435c-ae57-45f458f15896",
          "type": "basic.info",
          "data": {
            "info": "NOT LRCK =>\nLRCK for left\nchannel",
            "readonly": true
          },
          "position": {
            "x": -376,
            "y": -128
          },
          "size": {
            "width": 120,
            "height": 64
          }
        },
        {
          "id": "738831ae-8ed7-4191-8455-eb6184ef5653",
          "type": "basic.info",
          "data": {
            "info": "LRCK =>\nLRCK for right\nchannel",
            "readonly": true
          },
          "position": {
            "x": -632,
            "y": -104
          },
          "size": {
            "width": 128,
            "height": 64
          }
        },
        {
          "id": "e8971ff7-c2f4-40b6-a8e0-a470ecf5878d",
          "type": "217930d697df9bce636b3abee451577565c90d0a",
          "position": {
            "x": -464,
            "y": 224
          },
          "size": {
            "width": 96,
            "height": 96
          }
        },
        {
          "id": "b22d8fb4-1526-4591-9b0a-3b3a51dbc992",
          "type": "217930d697df9bce636b3abee451577565c90d0a",
          "position": {
            "x": -464,
            "y": 472
          },
          "size": {
            "width": 96,
            "height": 96
          }
        },
        {
          "id": "fb202f0f-65c8-43c4-9247-3b99a827e9ea",
          "type": "dfa7edc9373492388c38c768173e06aea82860d6",
          "position": {
            "x": -208,
            "y": 64
          },
          "size": {
            "width": 96,
            "height": 64
          }
        },
        {
          "id": "dad9a083-d0f4-47de-ad19-1cdbe26f7687",
          "type": "basic.info",
          "data": {
            "info": "LRCK delayed by 1 MCK\nfor latching into serial \noutput registers",
            "readonly": true
          },
          "position": {
            "x": -80,
            "y": 40
          },
          "size": {
            "width": 200,
            "height": 104
          }
        },
        {
          "id": "21073813-69a4-4b5a-8a96-428a0843fbcb",
          "type": "477177abb0f584a4b351d17e1de69e6945bfdbcd",
          "position": {
            "x": -192,
            "y": 480
          },
          "size": {
            "width": 96,
            "height": 96
          }
        },
        {
          "id": "a04a374f-6593-4f2d-9d0f-7802e570ba76",
          "type": "basic.info",
          "data": {
            "info": "2x 32bit shift register\nwith serial input and\nparallel output on latch",
            "readonly": true
          },
          "position": {
            "x": -560,
            "y": 352
          },
          "size": {
            "width": 192,
            "height": 128
          }
        },
        {
          "id": "4e2469c5-220f-497e-a743-b1faa12ab8b6",
          "type": "477177abb0f584a4b351d17e1de69e6945bfdbcd",
          "position": {
            "x": -192,
            "y": 232
          },
          "size": {
            "width": 96,
            "height": 96
          }
        }
      ],
      "wires": [
        {
          "source": {
            "block": "5b5f2c70-89c8-4f1c-bb2e-99891dbac1ef",
            "port": "out"
          },
          "target": {
            "block": "671b4818-4d67-4d71-b056-e32771954a6d",
            "port": "18c2ebc7-5152-439c-9b3f-851c59bac834"
          },
          "vertices": [
            {
              "x": -584,
              "y": -56
            }
          ]
        },
        {
          "source": {
            "block": "53cf65c7-3c3d-4f50-99d3-c42fb4dfef36",
            "port": "out"
          },
          "target": {
            "block": "e8971ff7-c2f4-40b6-a8e0-a470ecf5878d",
            "port": "4f33eacd-d0a7-4574-8800-d3b32b422580"
          }
        },
        {
          "source": {
            "block": "9c905044-cfbc-415d-8ced-081c1c4a06a3",
            "port": "out"
          },
          "target": {
            "block": "e8971ff7-c2f4-40b6-a8e0-a470ecf5878d",
            "port": "525c1541-365e-485b-82d4-1946acae1069"
          },
          "vertices": [
            {
              "x": -624,
              "y": 200
            }
          ]
        },
        {
          "source": {
            "block": "9c905044-cfbc-415d-8ced-081c1c4a06a3",
            "port": "out"
          },
          "target": {
            "block": "b22d8fb4-1526-4591-9b0a-3b3a51dbc992",
            "port": "525c1541-365e-485b-82d4-1946acae1069"
          },
          "vertices": [
            {
              "x": -624,
              "y": 400
            }
          ]
        },
        {
          "source": {
            "block": "5b5f2c70-89c8-4f1c-bb2e-99891dbac1ef",
            "port": "out"
          },
          "target": {
            "block": "b22d8fb4-1526-4591-9b0a-3b3a51dbc992",
            "port": "4ffbf419-dc95-47cd-8731-3b15dee38989"
          },
          "vertices": [
            {
              "x": -600,
              "y": 424
            }
          ]
        },
        {
          "source": {
            "block": "12a66937-3452-4cb7-8712-72dd77287bce",
            "port": "out"
          },
          "target": {
            "block": "fb202f0f-65c8-43c4-9247-3b99a827e9ea",
            "port": "3943e194-090b-4553-9df3-88bc4b17abc2"
          }
        },
        {
          "source": {
            "block": "e8971ff7-c2f4-40b6-a8e0-a470ecf5878d",
            "port": "8a012405-7f39-478d-9f01-9f7be3cabae6"
          },
          "target": {
            "block": "b22d8fb4-1526-4591-9b0a-3b3a51dbc992",
            "port": "4f33eacd-d0a7-4574-8800-d3b32b422580"
          },
          "vertices": [
            {
              "x": -408,
              "y": 448
            },
            {
              "x": -520,
              "y": 456
            }
          ]
        },
        {
          "source": {
            "block": "b22d8fb4-1526-4591-9b0a-3b3a51dbc992",
            "port": "7a423093-735f-42db-920f-4a6543a4c475"
          },
          "target": {
            "block": "21073813-69a4-4b5a-8a96-428a0843fbcb",
            "port": "bd769860-0ed7-4e7d-bd1e-f922cc39ee22"
          },
          "size": 32
        },
        {
          "source": {
            "block": "9c905044-cfbc-415d-8ced-081c1c4a06a3",
            "port": "out"
          },
          "target": {
            "block": "21073813-69a4-4b5a-8a96-428a0843fbcb",
            "port": "031afb48-d6ec-4994-b07d-961f865cc3b8"
          },
          "vertices": [
            {
              "x": -280,
              "y": 424
            }
          ]
        },
        {
          "source": {
            "block": "fb202f0f-65c8-43c4-9247-3b99a827e9ea",
            "port": "aa84d31e-cd92-44c7-bb38-c7a4cd903a78"
          },
          "target": {
            "block": "21073813-69a4-4b5a-8a96-428a0843fbcb",
            "port": "b546f314-d238-4bc4-954e-014fc09f612b"
          },
          "vertices": [
            {
              "x": -8,
              "y": 440
            }
          ]
        },
        {
          "source": {
            "block": "21073813-69a4-4b5a-8a96-428a0843fbcb",
            "port": "ce3ba22e-b85f-4c86-a6b5-5bc964bf3293"
          },
          "target": {
            "block": "eaa8f799-4f24-453b-8d0d-bcccb74d566a",
            "port": "in"
          }
        },
        {
          "source": {
            "block": "21073813-69a4-4b5a-8a96-428a0843fbcb",
            "port": "e5a73377-e314-4ff8-848f-33ba6ecb09de"
          },
          "target": {
            "block": "7e1734b3-7da2-4749-89b0-e3ae3f38cc61",
            "port": "in"
          }
        },
        {
          "source": {
            "block": "5b5f2c70-89c8-4f1c-bb2e-99891dbac1ef",
            "port": "out"
          },
          "target": {
            "block": "fb202f0f-65c8-43c4-9247-3b99a827e9ea",
            "port": "bf2f0c53-2d04-4cba-aa70-2df85502d24f"
          },
          "vertices": [
            {
              "x": -520,
              "y": 80
            }
          ]
        },
        {
          "source": {
            "block": "671b4818-4d67-4d71-b056-e32771954a6d",
            "port": "664caf9e-5f40-4df4-800a-b626af702e62"
          },
          "target": {
            "block": "e8971ff7-c2f4-40b6-a8e0-a470ecf5878d",
            "port": "4ffbf419-dc95-47cd-8731-3b15dee38989"
          },
          "vertices": [
            {
              "x": -352,
              "y": 144
            },
            {
              "x": -496,
              "y": 152
            }
          ]
        },
        {
          "source": {
            "block": "e8971ff7-c2f4-40b6-a8e0-a470ecf5878d",
            "port": "7a423093-735f-42db-920f-4a6543a4c475"
          },
          "target": {
            "block": "4e2469c5-220f-497e-a743-b1faa12ab8b6",
            "port": "bd769860-0ed7-4e7d-bd1e-f922cc39ee22"
          },
          "size": 32
        },
        {
          "source": {
            "block": "9c905044-cfbc-415d-8ced-081c1c4a06a3",
            "port": "out"
          },
          "target": {
            "block": "4e2469c5-220f-497e-a743-b1faa12ab8b6",
            "port": "031afb48-d6ec-4994-b07d-961f865cc3b8"
          },
          "vertices": [
            {
              "x": -280,
              "y": 224
            }
          ]
        },
        {
          "source": {
            "block": "fb202f0f-65c8-43c4-9247-3b99a827e9ea",
            "port": "aa84d31e-cd92-44c7-bb38-c7a4cd903a78"
          },
          "target": {
            "block": "4e2469c5-220f-497e-a743-b1faa12ab8b6",
            "port": "b546f314-d238-4bc4-954e-014fc09f612b"
          },
          "vertices": [
            {
              "x": -8,
              "y": 336
            }
          ]
        },
        {
          "source": {
            "block": "4e2469c5-220f-497e-a743-b1faa12ab8b6",
            "port": "ce3ba22e-b85f-4c86-a6b5-5bc964bf3293"
          },
          "target": {
            "block": "8ef3e574-76c4-458d-8856-9083a04d7f8c",
            "port": "in"
          }
        },
        {
          "source": {
            "block": "4e2469c5-220f-497e-a743-b1faa12ab8b6",
            "port": "e5a73377-e314-4ff8-848f-33ba6ecb09de"
          },
          "target": {
            "block": "340fe403-9ce9-4d94-97db-e1cafbe5ee46",
            "port": "in"
          }
        }
      ]
    }
  },
  "dependencies": {
    "3676a00f3a70e406487ed14b901daf3e4984e63d": {
      "package": {
        "name": "NOT",
        "version": "2.0",
        "description": "NOT gate (Verilog implementation)",
        "author": "Jesús Arroyo, Juan González",
        "image": "%3Csvg%20xmlns=%22http://www.w3.org/2000/svg%22%20width=%22317.651%22%20height=%22194.058%22%20version=%221%22%3E%3Cpath%20d=%22M69.246%204l161.86%2093.027-161.86%2093.031V4z%22%20fill=%22none%22%20stroke=%22#000%22%20stroke-width=%228%22%20stroke-linejoin=%22round%22/%3E%3Cellipse%20cx=%22253.352%22%20cy=%2296.736%22%20rx=%2221.393%22%20ry=%2221.893%22%20fill=%22none%22%20stroke=%22#000%22%20stroke-width=%228%22%20stroke-linecap=%22round%22%20stroke-linejoin=%22round%22/%3E%3Cpath%20d=%22M4.057%2097.49h65.262m205.796%200h38.48%22%20fill=%22none%22%20stroke=%22#000%22%20stroke-width=%228%22%20stroke-linecap=%22round%22/%3E%3Ctext%20style=%22line-height:125%25%22%20x=%2281.112%22%20y=%22111.734%22%20transform=%22scale(.99532%201.0047)%22%20font-weight=%22400%22%20font-size=%2249.675%22%20font-family=%22sans-serif%22%20letter-spacing=%220%22%20word-spacing=%220%22%20fill=%22#00f%22%3E%3Ctspan%20x=%2281.112%22%20y=%22111.734%22%20style=%22-inkscape-font-specification:'sans-serif%20Bold'%22%20font-weight=%22700%22%3ENot%3C/tspan%3E%3C/text%3E%3C/svg%3E"
      },
      "design": {
        "graph": {
          "blocks": [
            {
              "id": "18c2ebc7-5152-439c-9b3f-851c59bac834",
              "type": "basic.input",
              "data": {
                "name": ""
              },
              "position": {
                "x": 112,
                "y": 72
              }
            },
            {
              "id": "664caf9e-5f40-4df4-800a-b626af702e62",
              "type": "basic.output",
              "data": {
                "name": ""
              },
              "position": {
                "x": 560,
                "y": 72
              }
            },
            {
              "id": "5365ed8c-e5db-4445-938f-8d689830ea5c",
              "type": "basic.code",
              "data": {
                "code": "//-- NOT Gate\nassign q = ~a;\n\n",
                "params": [],
                "ports": {
                  "in": [
                    {
                      "name": "a"
                    }
                  ],
                  "out": [
                    {
                      "name": "q"
                    }
                  ]
                }
              },
              "position": {
                "x": 256,
                "y": 48
              },
              "size": {
                "width": 256,
                "height": 104
              }
            },
            {
              "id": "e3bb41e3-1944-4946-9675-c2dbe2e49fcf",
              "type": "basic.info",
              "data": {
                "info": "Input",
                "readonly": true
              },
              "position": {
                "x": 128,
                "y": 32
              },
              "size": {
                "width": 80,
                "height": 40
              }
            },
            {
              "id": "8408dd5f-945f-4a89-9790-7752813d4e91",
              "type": "basic.info",
              "data": {
                "info": "Output",
                "readonly": true
              },
              "position": {
                "x": 576,
                "y": 40
              },
              "size": {
                "width": 80,
                "height": 40
              }
            }
          ],
          "wires": [
            {
              "source": {
                "block": "18c2ebc7-5152-439c-9b3f-851c59bac834",
                "port": "out"
              },
              "target": {
                "block": "5365ed8c-e5db-4445-938f-8d689830ea5c",
                "port": "a"
              }
            },
            {
              "source": {
                "block": "5365ed8c-e5db-4445-938f-8d689830ea5c",
                "port": "q"
              },
              "target": {
                "block": "664caf9e-5f40-4df4-800a-b626af702e62",
                "port": "in"
              }
            }
          ]
        }
      }
    },
    "217930d697df9bce636b3abee451577565c90d0a": {
      "package": {
        "name": "",
        "version": "",
        "description": "",
        "author": "",
        "image": ""
      },
      "design": {
        "graph": {
          "blocks": [
            {
              "id": "525c1541-365e-485b-82d4-1946acae1069",
              "type": "basic.input",
              "data": {
                "name": "CLK",
                "clock": true
              },
              "position": {
                "x": 296,
                "y": -112
              }
            },
            {
              "id": "7a423093-735f-42db-920f-4a6543a4c475",
              "type": "basic.output",
              "data": {
                "name": "DOUT",
                "range": "[31:0]",
                "size": 32
              },
              "position": {
                "x": 1120,
                "y": -80
              }
            },
            {
              "id": "4f33eacd-d0a7-4574-8800-d3b32b422580",
              "type": "basic.input",
              "data": {
                "name": "SDI",
                "clock": false
              },
              "position": {
                "x": 296,
                "y": 24
              }
            },
            {
              "id": "8a012405-7f39-478d-9f01-9f7be3cabae6",
              "type": "basic.output",
              "data": {
                "name": "SDO"
              },
              "position": {
                "x": 1120,
                "y": 128
              }
            },
            {
              "id": "4ffbf419-dc95-47cd-8731-3b15dee38989",
              "type": "basic.input",
              "data": {
                "name": "LATCH",
                "clock": false
              },
              "position": {
                "x": 296,
                "y": 160
              }
            },
            {
              "id": "46c50af6-adb1-405b-9a6a-526b274a0d6a",
              "type": "basic.code",
              "data": {
                "ports": {
                  "in": [
                    {
                      "name": "CLK"
                    },
                    {
                      "name": "SDI"
                    },
                    {
                      "name": "LATCH"
                    }
                  ],
                  "out": [
                    {
                      "name": "DOUT",
                      "range": "[31:0]",
                      "size": 32
                    },
                    {
                      "name": "SDO"
                    }
                  ]
                },
                "params": [],
                "code": "reg [31:0] shreg = 0;\nreg [31:0] outreg = 0;\n\nalways @(posedge CLK)\nbegin\n    shreg = {shreg[30:0], SDI};\nend\n\nalways @(posedge LATCH)\nbegin\n    outreg = shreg;\nend\n\nassign SDO = shreg[31];\nassign DOUT = outreg;\n"
              },
              "position": {
                "x": 480,
                "y": -152
              },
              "size": {
                "width": 536,
                "height": 416
              }
            }
          ],
          "wires": [
            {
              "source": {
                "block": "4ffbf419-dc95-47cd-8731-3b15dee38989",
                "port": "out"
              },
              "target": {
                "block": "46c50af6-adb1-405b-9a6a-526b274a0d6a",
                "port": "LATCH"
              }
            },
            {
              "source": {
                "block": "4f33eacd-d0a7-4574-8800-d3b32b422580",
                "port": "out"
              },
              "target": {
                "block": "46c50af6-adb1-405b-9a6a-526b274a0d6a",
                "port": "SDI"
              }
            },
            {
              "source": {
                "block": "525c1541-365e-485b-82d4-1946acae1069",
                "port": "out"
              },
              "target": {
                "block": "46c50af6-adb1-405b-9a6a-526b274a0d6a",
                "port": "CLK"
              }
            },
            {
              "source": {
                "block": "46c50af6-adb1-405b-9a6a-526b274a0d6a",
                "port": "DOUT"
              },
              "target": {
                "block": "7a423093-735f-42db-920f-4a6543a4c475",
                "port": "in"
              },
              "size": 32
            },
            {
              "source": {
                "block": "46c50af6-adb1-405b-9a6a-526b274a0d6a",
                "port": "SDO"
              },
              "target": {
                "block": "8a012405-7f39-478d-9f01-9f7be3cabae6",
                "port": "in"
              }
            }
          ]
        }
      }
    },
    "dfa7edc9373492388c38c768173e06aea82860d6": {
      "package": {
        "name": "sys-DFF-verilog",
        "version": "2.3",
        "description": "System - D Flip-flop. Capture data every system clock cycle. Verilog implementation",
        "author": "Juan González-Gómez (Obijuan)",
        "image": "%3Csvg%20width=%22196.313%22%20height=%22216.83%22%20viewBox=%220%200%2051.941051%2057.369679%22%20xmlns=%22http://www.w3.org/2000/svg%22%3E%3Cg%20transform=%22translate(-52.22%20-48.028)%22%3E%3Crect%20width=%2224.412%22%20height=%2213.185%22%20x=%2279.352%22%20y=%2253.67%22%20ry=%222.247%22%20fill=%22#fff%22%20stroke=%22#000%22%20stroke-width=%22.794%22%20stroke-linecap=%22round%22/%3E%3Cg%20stroke=%22#000%22%20stroke-width=%221.442%22%20stroke-linecap=%22round%22%20stroke-linejoin=%22round%22%3E%3Cpath%20d=%22M77.902%2088.18l13.607%2016.672-6.918-20.534%22%20fill=%22#ccc%22%20stroke-width=%221.0924880399999999%22/%3E%3Cpath%20d=%22M70.517%2080.116l-9.232-19.613-6.45%203.725-2.07-3.584%2020.905-12.07%202.07%203.584-6.093%203.518%2012.03%2018.222s5.4-2.025%208.536.74c3.136%202.766%202.52%204.92%202.887%204.773L69.412%2093.049s-2.848-3.696-2.16-6.796c.687-3.1%203.265-6.137%203.265-6.137z%22%20fill=%22red%22%20stroke-width=%221.0924880399999999%22/%3E%3C/g%3E%3Ctext%20style=%22line-height:1.25%22%20x=%2281.296%22%20y=%2263.239%22%20font-weight=%22400%22%20font-size=%2210.583%22%20font-family=%22sans-serif%22%20fill=%22#00f%22%20stroke-width=%22.265%22%3E%3Ctspan%20style=%22-inkscape-font-specification:'sans-serif%20Bold'%22%20x=%2281.296%22%20y=%2263.239%22%20font-weight=%22700%22%3ESys%3C/tspan%3E%3C/text%3E%3C/g%3E%3C/svg%3E"
      },
      "design": {
        "graph": {
          "blocks": [
            {
              "id": "54dbabeb-8aef-4184-8fdc-87528aca29a3",
              "type": "basic.output",
              "data": {
                "name": "nc"
              },
              "position": {
                "x": 816,
                "y": 112
              }
            },
            {
              "id": "3943e194-090b-4553-9df3-88bc4b17abc2",
              "type": "basic.input",
              "data": {
                "name": "",
                "clock": true
              },
              "position": {
                "x": 208,
                "y": 160
              }
            },
            {
              "id": "aa84d31e-cd92-44c7-bb38-c7a4cd903a78",
              "type": "basic.output",
              "data": {
                "name": ""
              },
              "position": {
                "x": 816,
                "y": 224
              }
            },
            {
              "id": "bf2f0c53-2d04-4cba-aa70-2df85502d24f",
              "type": "basic.input",
              "data": {
                "name": "",
                "clock": false
              },
              "position": {
                "x": 208,
                "y": 304
              }
            },
            {
              "id": "65194b18-5d2a-41b2-bd86-01be99978ad6",
              "type": "basic.constant",
              "data": {
                "name": "",
                "value": "0",
                "local": false
              },
              "position": {
                "x": 512,
                "y": 64
              }
            },
            {
              "id": "bdc170f0-4468-4137-bd79-4624c9cadf2b",
              "type": "basic.code",
              "data": {
                "code": "//-- Initial value\nreg q = INI;\n\n//-- Capture the input data  \n//-- on the rising edge of  \n//-- the system clock\nalways @(posedge clk)\n  q <= d;",
                "params": [
                  {
                    "name": "INI"
                  }
                ],
                "ports": {
                  "in": [
                    {
                      "name": "clk"
                    },
                    {
                      "name": "d"
                    }
                  ],
                  "out": [
                    {
                      "name": "q"
                    }
                  ]
                }
              },
              "position": {
                "x": 384,
                "y": 168
              },
              "size": {
                "width": 344,
                "height": 176
              }
            },
            {
              "id": "53d11290-50b3-40fb-b253-222cb296b075",
              "type": "basic.info",
              "data": {
                "info": "Parameter: Initial value",
                "readonly": true
              },
              "position": {
                "x": 488,
                "y": 32
              },
              "size": {
                "width": 208,
                "height": 40
              }
            },
            {
              "id": "c25a29cd-d5ed-435e-b375-e6d5557660d8",
              "type": "basic.info",
              "data": {
                "info": "System clock",
                "readonly": true
              },
              "position": {
                "x": 208,
                "y": 136
              },
              "size": {
                "width": 120,
                "height": 32
              }
            },
            {
              "id": "ecafc6fa-330b-4ba7-aa67-40b3ea48f1f1",
              "type": "basic.info",
              "data": {
                "info": "Input data",
                "readonly": true
              },
              "position": {
                "x": 224,
                "y": 280
              },
              "size": {
                "width": 112,
                "height": 40
              }
            },
            {
              "id": "df95c331-682d-4733-a62d-ad9fcd75f96a",
              "type": "basic.info",
              "data": {
                "info": "Output",
                "readonly": true
              },
              "position": {
                "x": 840,
                "y": 200
              },
              "size": {
                "width": 80,
                "height": 40
              }
            },
            {
              "id": "dd8217df-b56d-49a9-ae94-f5e0c96e1460",
              "type": "basic.info",
              "data": {
                "info": "# D Flip-Flop  (system)\n\nIt stores the input data that arrives at cycle n  \nIts output is shown in the cycle n+1",
                "readonly": true
              },
              "position": {
                "x": 144,
                "y": -136
              },
              "size": {
                "width": 488,
                "height": 104
              }
            },
            {
              "id": "92bfbcf5-6016-4ad8-963c-c5c7747304d0",
              "type": "basic.info",
              "data": {
                "info": "Not connected",
                "readonly": true
              },
              "position": {
                "x": 808,
                "y": 88
              },
              "size": {
                "width": 176,
                "height": 32
              }
            }
          ],
          "wires": [
            {
              "source": {
                "block": "3943e194-090b-4553-9df3-88bc4b17abc2",
                "port": "out"
              },
              "target": {
                "block": "bdc170f0-4468-4137-bd79-4624c9cadf2b",
                "port": "clk"
              }
            },
            {
              "source": {
                "block": "bdc170f0-4468-4137-bd79-4624c9cadf2b",
                "port": "q"
              },
              "target": {
                "block": "aa84d31e-cd92-44c7-bb38-c7a4cd903a78",
                "port": "in"
              }
            },
            {
              "source": {
                "block": "65194b18-5d2a-41b2-bd86-01be99978ad6",
                "port": "constant-out"
              },
              "target": {
                "block": "bdc170f0-4468-4137-bd79-4624c9cadf2b",
                "port": "INI"
              }
            },
            {
              "source": {
                "block": "bf2f0c53-2d04-4cba-aa70-2df85502d24f",
                "port": "out"
              },
              "target": {
                "block": "bdc170f0-4468-4137-bd79-4624c9cadf2b",
                "port": "d"
              }
            }
          ]
        }
      }
    },
    "477177abb0f584a4b351d17e1de69e6945bfdbcd": {
      "package": {
        "name": "",
        "version": "",
        "description": "",
        "author": "",
        "image": ""
      },
      "design": {
        "graph": {
          "blocks": [
            {
              "id": "bd769860-0ed7-4e7d-bd1e-f922cc39ee22",
              "type": "basic.input",
              "data": {
                "name": "DIN",
                "range": "[31:0]",
                "clock": false,
                "size": 32
              },
              "position": {
                "x": 336,
                "y": 296
              }
            },
            {
              "id": "ce3ba22e-b85f-4c86-a6b5-5bc964bf3293",
              "type": "basic.output",
              "data": {
                "name": "SDOP"
              },
              "position": {
                "x": 1160,
                "y": 336
              }
            },
            {
              "id": "031afb48-d6ec-4994-b07d-961f865cc3b8",
              "type": "basic.input",
              "data": {
                "name": "CLK",
                "clock": true
              },
              "position": {
                "x": 344,
                "y": 448
              }
            },
            {
              "id": "e5a73377-e314-4ff8-848f-33ba6ecb09de",
              "type": "basic.output",
              "data": {
                "name": "SDON"
              },
              "position": {
                "x": 1160,
                "y": 568
              }
            },
            {
              "id": "b546f314-d238-4bc4-954e-014fc09f612b",
              "type": "basic.input",
              "data": {
                "name": "LATCH",
                "clock": false
              },
              "position": {
                "x": 352,
                "y": 600
              }
            },
            {
              "id": "5d05b506-fb60-46f1-bcde-004c732834cc",
              "type": "basic.code",
              "data": {
                "ports": {
                  "in": [
                    {
                      "name": "DIN",
                      "range": "[31:0]",
                      "size": 32
                    },
                    {
                      "name": "CLK"
                    },
                    {
                      "name": "LATCH"
                    }
                  ],
                  "out": [
                    {
                      "name": "SDOP"
                    },
                    {
                      "name": "SDON"
                    }
                  ]
                },
                "params": [],
                "code": "reg [31:0] shreg = 0;\ninteger idx = 31;\nbool zerostuff = 0;\nreg sdopos, sdoneg;\n\nalways @(posedge LATCH)\n    shreg = DIN;\n\nalways @(posedge CLK)\nbegin\n    if (!zerostuff) begin\n        sdopos = shreg[idx];\n        sdoneg = ~sdopos;\n        if (idx == 0) begin\n            idx = 31;\n        end else begin\n            idx = idx - 1;\n        end\n    end else begin\n        sdopos = 0;\n        sdoneg = 0;\n    end\n    zerostuff = ~zerostuff;\nend\n\nassign SDOP = sdopos;\nassign SDON = sdoneg;\n"
              },
              "position": {
                "x": 536,
                "y": 248
              },
              "size": {
                "width": 560,
                "height": 464
              }
            }
          ],
          "wires": [
            {
              "source": {
                "block": "b546f314-d238-4bc4-954e-014fc09f612b",
                "port": "out"
              },
              "target": {
                "block": "5d05b506-fb60-46f1-bcde-004c732834cc",
                "port": "LATCH"
              }
            },
            {
              "source": {
                "block": "031afb48-d6ec-4994-b07d-961f865cc3b8",
                "port": "out"
              },
              "target": {
                "block": "5d05b506-fb60-46f1-bcde-004c732834cc",
                "port": "CLK"
              }
            },
            {
              "source": {
                "block": "bd769860-0ed7-4e7d-bd1e-f922cc39ee22",
                "port": "out"
              },
              "target": {
                "block": "5d05b506-fb60-46f1-bcde-004c732834cc",
                "port": "DIN"
              },
              "size": 32
            },
            {
              "source": {
                "block": "5d05b506-fb60-46f1-bcde-004c732834cc",
                "port": "SDON"
              },
              "target": {
                "block": "e5a73377-e314-4ff8-848f-33ba6ecb09de",
                "port": "in"
              }
            },
            {
              "source": {
                "block": "5d05b506-fb60-46f1-bcde-004c732834cc",
                "port": "SDOP"
              },
              "target": {
                "block": "ce3ba22e-b85f-4c86-a6b5-5bc964bf3293",
                "port": "in"
              }
            }
          ]
        }
      }
    }
  }
}